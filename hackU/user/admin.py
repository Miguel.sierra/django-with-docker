from django.contrib import admin

# Register your models here.
from user.models import user


@admin.register(user)
class UserAdmin(admin.ModelAdmin):

    list_display = ('user','phone_number')
