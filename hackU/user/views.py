"""Views."""
from django.http import HttpResponse
from django.http import JsonResponse
from django.shortcuts import redirect
from django.shortcuts import render
from user.forms.user import RegistrarUsuario
# Create your views here.

def example(request):
    if request.method == 'POST':
        register_form = RegistrarUsuario(request.POST)
        if register_form.is_valid():
            success = register_form.registrar()
            return redirect('./')
    else:
        register_form = RegistrarUsuario()
        return render(request, 'forms/signup.html', {'register_form': register_form})
