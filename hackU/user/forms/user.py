"""User forms."""

# Django
from django import forms
from django.contrib.auth.models import User

# Models
from user.models import user

class RegistrarUsuario(forms.Form):
    phone_number = forms.CharField(label='Phone number:',max_length=15)

    first_name = forms.CharField(
        min_length=2,
        max_length=50,
        label='',
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'First Name'
                }
        )
    )

    last_name = forms.CharField(
        min_length=2,
        max_length=50,
        label='',
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'Last Name'
            }
        )
    )

    username = forms.CharField(
        min_length=4,
        max_length=50,
        label='',
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'Nickname'
            }
        )
    )
    phone_number = forms.CharField(
        min_length=2,
        max_length=50,
        label='',
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'Phone Number'
            }
        )
    )

    email = forms.CharField(
        min_length=6,
        max_length=70,
        label='',
        widget=forms.EmailInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'Email'
                }
        )
    )

    password = forms.CharField(
        min_length=7,
        max_length=20,
        label='',
        widget=forms.PasswordInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'Password'
                }
        )
    )

    password_confirmation = forms.CharField(
        min_length=7,
        max_length=20,
        label='',
        widget=forms.PasswordInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'Password Confirmation'
                }
        )
    )

    def registrar(self):
        data = self.cleaned_data
        data.pop('password_confirmation')
        phone_number = data['phone_number']
        data.pop('phone_number')
        new_user = User(**data)
        new_user.save()
        new_phone = user(user=new_user,phone_number=phone_number)
        new_phone.save()
        return new_user