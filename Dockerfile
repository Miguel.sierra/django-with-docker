FROM python:3
ENV PYTHONUNBUFFERED 1
RUN mkdir /code
WORKDIR /code
COPY /requeriments/local.txt /code/
RUN pip install -r local.txt
COPY . /code/